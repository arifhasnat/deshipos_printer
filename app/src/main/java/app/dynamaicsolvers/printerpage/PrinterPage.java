package app.dynamaicsolvers.printerpage;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import app.dynamaicsolvers.printerpage.Adapter.PrintPageAdapter;
import app.dynamaicsolvers.printerpage.Model.InvoiceDataModel;
import app.dynamaicsolvers.printerpage.Model.SaleModel;

public class PrinterPage extends AppCompatActivity {
    TextView shopName,address,phone,date;
    ImageView imageView;

    TextView total,less,discount,net;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    SaleModel saleModels;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        shopName = (TextView) findViewById(R.id.textshopName);
        address = (TextView) findViewById(R.id.Address);
        phone= (TextView) findViewById(R.id.phnNo);
        date = (TextView) findViewById(R.id.date);
        imageView= (ImageView) findViewById(R.id.imageViewIcon);
        recyclerView= (RecyclerView) findViewById(R.id.salesRecycler);

        shopName.setText("রাজশাহীর আমের দোকান");
        address.setText("কালশি রোড-মিরপুর-১২");
        phone.setText("০১৮২৭৩৮৮১৭৮");
        date.setText("৬/০৫/২০১৬");

   /*     ArrayList<InvoiceDataModel> allInvoiceDataModel=new ArrayList<>();
        for (int i =0 ; i<5 ; i++){
            InvoiceDataModel invoiceDataModel = new InvoiceDataModel("image"+i, i , "name : "+i, i+1, i*20.0, i*20.0,i*20 );
            allInvoiceDataModel.add(invoiceDataModel);
        }


        SaleModel saleModel = new SaleModel(allInvoiceDataModel);


        ArrayList<SaleModel> allSaleModels=new ArrayList<>();

        allSaleModels.add(saleModel);*/

        InvoiceDataModel itemData[] = {new InvoiceDataModel(1,"Food Type One",3,78.0),
                new InvoiceDataModel(2,"Food Type One",3,78.0),
                new InvoiceDataModel(3,"Food Type One",3,78.0),
                new InvoiceDataModel(4,"Food Type One",3,78.0),
                new InvoiceDataModel(5,"Food Type One",3,78.0),
                new InvoiceDataModel(6,"Food Type One",3,78.0),
                new InvoiceDataModel(7,"Food Type One",3,78.0),};


        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new PrintPageAdapter(itemData);
        recyclerView.setAdapter(mAdapter);


    }


}
