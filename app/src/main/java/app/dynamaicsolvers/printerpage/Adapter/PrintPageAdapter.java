package app.dynamaicsolvers.printerpage.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import app.dynamaicsolvers.printerpage.Model.InvoiceDataModel;
import app.dynamaicsolvers.printerpage.Model.SaleModel;
import app.dynamaicsolvers.printerpage.R;

/**
 * Created by dynamicsolversbangladesh1 on 5/6/16.
 */
public class PrintPageAdapter extends RecyclerView.Adapter<PrintPageAdapter.PrintPageHolder>{

    InvoiceDataModel invoiceDataModel[];

    public PrintPageAdapter(InvoiceDataModel[] invoiceDataModel) {
        this.invoiceDataModel = invoiceDataModel;
    }

    @Override
    public PrintPageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layoutforrecycler, parent, false);

        PrintPageHolder printPageHolder = new PrintPageHolder(v);
        return printPageHolder;

    }

    @Override
    public void onBindViewHolder(PrintPageHolder holder, int position) {
        holder.textViewSerial.setText(Integer.valueOf(invoiceDataModel[position].id).toString());
        holder.textViewProductName.setText(invoiceDataModel[position].name);
        holder.textViewQuantity.setText(Integer.toString(invoiceDataModel[position].quantity).toString());
        holder.textViewPrice.setText(Double.toString(invoiceDataModel[position].price).toString());

    }

    @Override
    public int getItemCount() {
        return invoiceDataModel.length;
    }

    public class PrintPageHolder extends RecyclerView.ViewHolder {



        TextView textViewSerial,textViewProductName,textViewQuantity,textViewPrice;

        public PrintPageHolder(View itemView) {
            super(itemView);
            textViewSerial = (TextView) itemView.findViewById(R.id.serial);
            textViewProductName= (TextView) itemView.findViewById(R.id.productName);
            textViewQuantity = (TextView) itemView.findViewById(R.id.quantity);
            textViewPrice= (TextView) itemView.findViewById(R.id.price);


        }
    }
}
