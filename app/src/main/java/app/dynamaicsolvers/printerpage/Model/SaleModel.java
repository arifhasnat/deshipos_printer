package app.dynamaicsolvers.printerpage.Model;

import java.util.ArrayList;

/**
 * Created by dynamicsolversbangladesh1 on 5/6/16.
 */
public class SaleModel {

    private ArrayList<InvoiceDataModel> dataModels;
    private double allTotal;
    private double subTotal;
    private String userName;
    private String ecr;

    public SaleModel(ArrayList<InvoiceDataModel> dataModels) {
        this.dataModels = dataModels;
    }

    public SaleModel(ArrayList<InvoiceDataModel> dataModels, String ecr, String userName, double subTotal, double allTotal) {
        this.dataModels = dataModels;
        this.ecr = ecr;
        this.userName = userName;
        this.subTotal = subTotal;
        this.allTotal = allTotal;
    }

    public ArrayList<InvoiceDataModel> getDataModels() {
        return dataModels;
    }

    public void setDataModels(ArrayList<InvoiceDataModel> dataModels) {
        this.dataModels = dataModels;
    }

    public String getEcr() {
        return ecr;
    }

    public void setEcr(String ecr) {
        this.ecr = ecr;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getAllTotal() {
        return allTotal;
    }

    public void setAllTotal(double allTotal) {
        this.allTotal = allTotal;
    }
}
