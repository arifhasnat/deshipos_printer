package app.dynamaicsolvers.printerpage.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dynamicsolversbangladesh1 on 5/6/16.
 */
public class InvoiceDataModel implements Parcelable {
    public String imageURL;
    public int id;
    public String name;
    public int quantity;
    public double each;
    public double total;
    public double price;

    public InvoiceDataModel(int id, String name, int quantity, double price) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public InvoiceDataModel(){}

    public InvoiceDataModel(String imageURL, int id, String name, int quantity, double each, double total, double price) {
        this.imageURL = imageURL;
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.each = each;
        this.total = total;
        this.price = price;
    }

    protected InvoiceDataModel(Parcel in) {
        imageURL= in.readString();
        id=in.readInt();
        name = in.readString();
        quantity = in.readInt();
        each = in.readDouble();
        total = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imageURL);
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(quantity);
        dest.writeDouble(each);
        dest.writeDouble(total);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<InvoiceDataModel> CREATOR = new Parcelable.Creator<InvoiceDataModel>() {
        @Override
        public InvoiceDataModel createFromParcel(Parcel in) {
            return new InvoiceDataModel(in);
        }

        @Override
        public InvoiceDataModel[] newArray(int size) {
            return new InvoiceDataModel[size];
        }
    };
}
